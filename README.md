# Introdução ao Machine Learning - SEnC 18

Material do mini-curso de Machine Learning ministrado na [Semana de Engenharia da Computação 2018](https://senc.icmc.usp.br/) (USP São Carlos).

[Slides no Google Docs](https://docs.google.com/presentation/d/1X6zJCiJbOZ9caGGcnZ0vZ7liZDCAcd1LY-rLrrbGpCo)

### Notebooks no Google Colab

[Tutorial Google Colab](https://medium.com/deep-learning-turkey/google-colab-free-gpu-tutorial-e113627b9f5d)

* [KNN](https://colab.research.google.com/drive/1sWSFfkx39_c00tEPbh5UZxmy2j2Mp7R3)
* [Regressão Linear](https://colab.research.google.com/drive/1P5K1spJnlHYPfWnDnOOUWxMIfg4Qjm9V)
* [Regressão Logística](https://colab.research.google.com/drive/1F8RBvQPcuSG93ZcOf_X3-xWqbPHkJRMX)
* [Redes Neurais Convolucionais](https://colab.research.google.com/drive/1xz9nI-JHrMMK-RNwUtz_J8fOw4m-q_Jv)
